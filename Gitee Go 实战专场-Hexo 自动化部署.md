![输入图片说明](https://images.gitee.com/uploads/images/2020/1122/150504_9ea4310b_6544037.jpeg "画板备份-2-1.jpg")

在上次直播中为大家介绍了 Gitee 自研 CI/CD 工具 Gitee Go 它的基本用法及简单案例

许多用户朋友对其更多的应用及实战表示很感兴趣

那么本周我们趁热打铁

从实战出发,为你带来 

**Hexo 博客在 Gitee 上的自动化部署**

### 直播时间

12 月 23 日晚 19:30-20:30
### 讲师

Gitee Go 产品经理 张盛翔

### 互动福利

参与直播互动可获得企业版优惠，还有机会获赠奖品，文化衫、鼠标垫等礼品等你拿。

## 参与方式

扫描下方二维码，加入官方直播群，群内将为大家推送直播链接。
![输入图片说明](https://images.gitee.com/uploads/images/2020/1221/013522_d9c3f6b1_5694891.jpeg "QQ20201221-0.jpg")
