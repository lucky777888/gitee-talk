![输入图片说明](https://images.gitee.com/uploads/images/2020/1122/150504_9ea4310b_6544037.jpeg "画板备份-2-1.jpg")

Gitee自研 CI/CD 工具 Gitee Go 正式上线后，我们收到了许多用户的反馈。

经过近期不断地改进，现在我们把它介绍给更多 Gitee 用户。

**什么是 Gitee Go，它能做些什么？**

**如何使用 Gitee Go 把流水线真正地「跑起来」？**

即将到来的 Gitee Talk 第三期

**看 Gitee 产品经理带你玩转 Gitee Go，打造高效 CI/CD 流水线。**

### 直播时间

12 月 9 日晚 19:30-20:30
### 讲师

Gitee Go 产品经理 张盛翔

### 互动福利

参与直播互动可获得企业版优惠，还有机会获赠奖品，文化衫、鼠标垫等礼品等你拿。

## 参与方式

扫描下方二维码，加入官方直播群，群内将为大家推送直播链接。
![](https://oscimg.oschina.net/oscnet/up-e5061763d98c0b161c809164bcb9e41cc64.JPEG)